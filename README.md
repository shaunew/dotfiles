Dot Files
=========

These are dot files that configure my terminal environment.  They
are as important as the code that I write, so it is worth saving.

To recover:
```
./sync.sh
```

Resources
---------

Comments are placed where appropriate, except ctags doesn't allow them:

- ctags config source: https://gist.github.com/spicycode/5254493
